<div class="row d-flex justify-content-center">
    <div class="col-md-8 mt-5">

        <!-- Jumbotron -->
        <div class="jumbotron text-center">

        <!-- Title -->
        <h2 class="card-title h2">Relatório de Interação com o Usuário</h2>
        <!-- Subtitle -->
        <p class="orange-text my-4 font-weight-bold">Google Fitness API</p>

        <!-- Grid row -->
        <div class="row d-flex justify-content-center">

            <!-- Grid column -->
            <div class="col-xl-7 pb-2">

            <p class="card-text">A API Google Fitness permite que o usuário insira e acesse seus dados de saúde e de exercícios.
                Para isso, em teoria, o usuário necessita de uma conta do Google válida para utilizar da aplicação.
                Não apenas o Usuário, mas o desenvolvedor também, dado que a API requere chaves e ID's fornecidos pelo Google, 
                variando dependendo da necessidade do seu aplicativo.</p>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

        <hr class="my-4">

        <div class="pt-2">
            <a href="<?= base_url('at02/manoela') ?>"><button type="button" class="btn btn-orange waves-effect">Home</button></a>
            <a href="<?= base_url('at02/sobre') ?>"><button type="button" class="btn btn-outline-orange waves-effect">Google Fitness</button></a>
        </div>

        </div>
        <!-- Jumbotron -->

    </div>
</div>