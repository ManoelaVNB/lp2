<?php
class PanelData{
    //isola a lógica de acesso ao banco de dados
    //le as informações e retorna em forma de vetor

    //atributos
    private $db; //acesso ao banco de dados
    private $data; //vetor interno

    function __construct($id = null){
        $ci = & get_instance(); //executando um metodo que da acesso ao objeto global CodeIgniter
        $this->db = $ci->db; //dá aceso à conexão com o banco de dados

        if($id) $this->start($id);
    }

    private function start($id){
        $rs = $this->db->get_where('panel_data', "id = $id");
        //$v = $rs->result()[0];
        $v = $rs->row();

        foreach($v as $nome =>$valor){ //nome = nome da coluna do db, value = valor da coluna 
            $this->$nome = $valor;     //geração automatica de atributos da classe
                                       //cria um atributo com o mesmo nome das colunas da tabela
        }

    }

    public function getColor($index){
        $v = array('primary', 'secondary', 'danger', 'alert', 'success', 'info', 'warning', 'light');

        //if($index > 7) $index = 1;
        return $v[$index % 8]; //se receber um num maior qsete 
    }

    /** Retorna num registros da tabela panel_data
    * caso num seja nulo, retorna todos os registros
    * RECEBE: @param int | null num:  a quantidade de registros desejada
    * RETORNA: @return array (objeto);*/
    public function load($num = null){
        $n = $num ? $num : PHP_INT_MAX; 
        $rs = $this->db->get('panel_data', $n);
        $this->data = $rs->result();
        return $this->data;
    }

    /** Retorna num registros da tabela panel_data
    * caso num seja nulo, retorna todos os registros
    * RECEBE: @param int | null num:  a quantidade de registros desejada
    * RETORNA: @return array (associativo);
    *public function load($num = null){
    *    $n = $num ? $num : PHP_INT_MAX; 
    *    $rs = $this->db->get('panel_data', $n);
    *    $this->data = $rs->result_array();
    *    return $this->data;
    *}
    */

}
?>