<nav class="navbar navbar-expand-lg navbar-dark bg-dark">

  <a class="navbar-brand" href="#"><img src="<?= base_url('assets/img/logo1.png') ?>" height='30' alt=""></a>
  <!-- <a class="navbar-brand" href="#">Navbar</a> -->

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('at02/manoela') ?>">Home <span class="sr-only"></span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('at02/inserir') ?>">Inserir</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('at02/resultado') ?>">Resultados</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('at02/relatorio') ?>">Relatorio</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('at02/sobre') ?>">Google Fitness</a>
      </li>
    </ul>
  </div>
</nav>