<?php
defined('BASEPATH') or exit('No direct script access allowed');

class at02 extends CI_Controller{

    public function manoela(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->view('api/dados_aluno');
        
        $this->load->view('common/rodape');  
        $this->load->view('common/footer'); 
    }

    public function sobre(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->view('api/sobre_gfit');
        
        $this->load->view('common/rodape');  
        $this->load->view('common/footer'); 
    }

    public function inserir(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->model('api/GFitModel', 'model');
        $this->model->api();
        
        $this->load->view('common/rodape');  
        $this->load->view('common/footer'); 
    }

    public function resultado(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        
        echo 'RESULTADO';
        
        $this->load->view('common/rodape');  
        $this->load->view('common/footer'); 
    }

    public function relatorio(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->view('api/relatorio_usuario');
        
        $this->load->view('common/rodape');  
        $this->load->view('common/footer'); 
    }

}

?>