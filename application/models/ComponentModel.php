<?php
 defined('BASEPATH') or exit('No direct script access allowed');
 include APPPATH.'libraries/component/Panel.php'; //importando as classes
 include APPPATH.'libraries/component/PanelData.php'; 
 include APPPATH.'libraries/component/Table.php'; 
 include APPPATH.'libraries/storage.php'; //inclui a classe

 class ComponentModel extends CI_Model{

    public function panel_sample(){
        //$pd = new PanelData(); 
        //$v = $pd->load();
        //$html = '';

        //foreach($v as $panel_data){
        //    $panel = new Panel($panel_data);
        //    $html .= $panel->getHTML();
        //}
        //return $html;

        $html = '';

        for($i=0; $i<12; $i++){
            $pd = new PanelData($i + 1);
            $panel = new Panel($pd);
            $html .= $panel->getHTML();
        }
        return $html;
    }

    public function table_sample(){
        $this->load->library('Storage');
        $data = $this->storage->lista_setor();
        $label = array('id', 'Setor', 'Imagem', 'Título Card', 'Descrição',
         'Figura', 'Título Figura', 'Subtitulo');
        $tabela = new Table($data,$label);
        $tabela->setHeaderColor('secondary-color-dark');
        $tabela->useHeaderWhiteText();
        $tabela->useZebraTable();
        $tabela->useBorder();
        $tabela->useHover();
        $tabela->useSmallRow();
        return $tabela->getHTML();
    }

 }
?>