<?php

class Table{
    private $data;
    private $labels;

    private $header_bg;
    private $white_text;
    private $class_list = array('table');
    
    /**
     * Inicializa os atributos do objeto.
     * @param array data: matriz de dados da tabela
     * @param array labels: lista de rótulos das colunas da tabela
     */
    public function __construct($data, $labels){
        $this->data = $data;
        $this->labels = $labels;
    }

    // Gera o cabeçalho da tabela
    private function header(){
        $html = '<thead class="'.$this->header_bg.' '.$this->white_text.'"><tr>';

        foreach ($this->labels as $label){
            $html .= '<th scope="col">'.$label.'</th>';
        }

        $html .= '</tr></thead>';
        return $html;
    }

    //Gera o corpo da tabela
    private function body(){
        $html = '<tbody>';

        foreach ($this->data as $row){ //linhas
            $html .= '<tr>';

            foreach($row as $col){
                $html .= '<td>'.$col.'</td>';
            }

            $html .= '</tr>';
        }

        $html .= '</tbody>';
        return $html;
    }

    /**
     * Gera o código html da tabela.
     * @return string: código HTML
     */
    public function getHTML(){
        $html = '<table class="table '.$this->get_classes().'">';
        $html .= $this->header();
        $html .= $this->body();
        $html .= '</table>';
        return $html;
    }

    private function get_classes(){
        return implode(' ', $this->class_list);
    }

    public function setHeaderColor($color){
        $this->header_bg = $color;
    }

    public function useHeaderWhiteText(){
        $this->white_text = 'text-white';
    }

    public function useZebraTable(){
        $this->class_list[] = 'table-striped';
    }

    public function useBorder(){
        $this->class_list[] = 'table-bordered';
    }

    public function useHover(){
        $this->class_list[] = 'table-hover';
    }

    public function useSmallRow(){
        $this->class_list[] = 'table-sm';
    }
}

?>