<div class="row d-flex justify-content-center">
    <div class="col-md-8 mt-5">

        <!-- Jumbotron -->
        <div class="jumbotron text-center justify-content-center">

        <!-- Card image -->
        <div class="view overlay my-4">
            <img src="<?= base_url('assets/img/logo2_dark.jpg') ?>" class="img-fluid" alt="">
        </div>

        <h5 class="indigo-text h5 mb-4">Google Fitness</h5>

        <p class="card-text text-justify">O Google Fitness é uma API que permite que seus usuários gravem seus dados de saúde e acessá-los quando necessário.
             Assim, os usuários podem ver seu progresso e o quanto de exercício foi feito por um determinado período.
        </p>
        <p class="card-text text-justify">Dentre os dados que podem ser armazenados, temos por exemplo:
        </p>
        <ul class="text-justify">
            <li>Informções nutricionais</li>
            <li>Batimentos Cardíacos</li>
            <li>Quantidade de Passos</li>
            <li>Altura e Peso</li>
            <li>etc.</li>
        </ul>

        </div>
        <!-- Jumbotron -->

    </div>
</div>
