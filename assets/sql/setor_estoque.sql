-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 12-Mar-2019 às 06:17
-- Versão do servidor: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lp2`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `setor_estoque`
--

CREATE TABLE `setor_estoque` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `image` int(11) NOT NULL,
  `label` varchar(30) NOT NULL,
  `card_title` varchar(30) NOT NULL,
  `descr` varchar(250) NOT NULL,
  `figure` int(11) NOT NULL,
  `fig_title` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `setor_estoque`
--

INSERT INTO `setor_estoque` (`id`, `title`, `image`, `label`, `card_title`, `descr`, `figure`, `fig_title`) VALUES
(2, 'Produtos', 42, 'Botão', 'Boa Noite', 'Uma descrição muito descritiva', 45, 'Uma Figura');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `setor_estoque`
--
ALTER TABLE `setor_estoque`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `setor_estoque`
--
ALTER TABLE `setor_estoque`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
